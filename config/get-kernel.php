<?php

use App\Kernel;
use App\Helpers\Vars;

include __DIR__.'/bootstrap.php';
$kernel = new Kernel(Vars::APP_ENV(), (bool) Vars::APP_DEBUG());
$kernel->boot();
return $kernel;

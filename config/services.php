<?php /** @noinspection PhpFullyQualifiedNameUsageInspection */

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

function invoke(string $invokableClassName)
{
    return service($invokableClassName);
}

return function (ContainerConfigurator $container) {

    /*
     * Configuring parameters
     */

    $params = $container->parameters();

    /*
     * Configuring services
     */

    $services = $container->services();

    $services->defaults()
        ->public()
        ->autoconfigure()
        ->autowire();

    /**
     * Batch loaders
     */

    $services->load('App\\Factory\\', '../src/Factory/*');
    $services->load('App\\Subscribers\\', '../src/Subscribers/**/*');
    $services->load('App\\Controller\\', '../src/Controller/**/*');
    $services->load('App\\Console\\', '../src/Console/**/*');
    $services->load('App\\Service\\', '../src/Service/**/*Service.php');

    /**
     * Speciefic services
     */

    $services->set('mainConnection', \Doctrine\ORM\EntityManager::class)
        ->factory(invoke(\App\Factory\MainConnectionFactory::class));

    $services->set('mainEntityManager', \Doctrine\ORM\EntityManager::class)
        ->factory(invoke(\App\Factory\MainEntityManagerFactory::class));

    $services->set('mainDoctrineOrmSchema', \CodexSoft\DatabaseFirst\DoctrineOrmSchema::class)
        ->factory(invoke(\App\Factory\MainDoctrineOrmSchemaFactory::class));

    $services->set('backendJsonApiSchema', \CodexSoft\JsonApi\JsonApiSchema::class)
        ->factory(invoke(\App\Factory\JsonBackendApiSchemaFactory::class));

    $services->set(\CodexSoft\JsonApi\Form\Extensions\FormFieldDefaultValueExtension::class);
    $services->set(\CodexSoft\JsonApi\Form\Extensions\FormFieldExampleExtension::class);
    $services->set(\CodexSoft\JsonApi\Command\SwagenCommand::class);

    $defaults = [
        \CodexSoft\DatabaseFirst\DoctrineOrmSchema::class => 'mainDoctrineOrmSchema',
        \Doctrine\ORM\EntityManager::class => 'mainEntityManager',
        \Doctrine\DBAL\Connection::class => 'mainConnection',
    ];

    foreach ($defaults as $serviceClass => $serviceFactory) {
        $services->alias($serviceClass, $serviceFactory);
    }
};

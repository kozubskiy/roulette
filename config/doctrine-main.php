<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

$kernel = include __DIR__.'/get-kernel.php';
$entityManager = $kernel->getContainer()->get(\Doctrine\ORM\EntityManager::class);;
return ConsoleRunner::createHelperSet($entityManager);

# Roulette App

## Prerequisites

Debian-based OS x64
Installed Docker Engine & Docker Compose packages

There are 2 repositories:

This one is to run dockerized app via docker-compose
https://gitlab.com/kozubskiy/roulette.installer

This one is application code (Symfony)
https://gitlab.com/kozubskiy/roulette

## Quick start:

```shell script
git clone git@gitlab.com:kozubskiy/roulette.installer.git
cd roulette.installer
./install.sh
```

Application will be exposed to http://localhost:8001

## Customized installation

```shell script
git clone git@gitlab.com:kozubskiy/roulette.installer.git
cd roulette.installer
```

Now check out files `./.env.default` and `./.env.php-fpm.default`. Copy them to `./.env` and `./.env.php-fpm` respectively and customize for your needs. 

```ini
INSTALLER_PULL_CODE=[0|1] should ./install.sh pull code to app/volumes/code or not
APP_PORT=8001 to which port on local machine nginx will be exposed
DB_PORT=8001 to which port on local machine postgresql server will be exposed
APP_PATH=/absolute/path/to/local/roulette/repository # if you prefer pull its code separately
PGDATA_VOLUME=roulette_pgdata_volume # name of pgdata docker volume
APP_ENV=dev # set symfony environment
APP_DEBUG=1 # enable/disable symfony debugging mode
...
```

Download app code
`make download`

Create docker volume
`docker volume create roulette_pgdata_volume`

Start containers
`make start`

Install composer packages
`make composer-install`

Apply migrations on DB
`make migrate`

## Tools

To add new user run:

`./app/bin/console app:add-user x@y.z`

x@y.z - email, must be unique within users table

To check algorithm that runs roulette round try this:

`./app/bin/console app:round`

## Endpoints

`GET http://localhost:8001/roulette/statistics`

example response:

```json
{
    "usersPerRoundNum": [
        {
            "ord_num": 1,
            "users_count": 4
        },
        {
            "ord_num": 3,
            "users_count": 1
        },
        {
            "ord_num": 4,
            "users_count": 1
        },
        {
            "ord_num": 2,
            "users_count": 1
        }
    ],
    "mostActive": [
        {
            "user_id": 1,
            "rounds_count": 6,
            "turns_count": 14,
            "average_turns_per_round": 2.3333333333333335
        },
        {
            "user_id": 34,
            "rounds_count": 1,
            "turns_count": 5,
            "average_turns_per_round": 5
        }
    ]
}
```

`POST http://localhost:8001/roulette/turn`

example request body:

```json
{
    "userId": 34
}
```

example response body:
```json
{
    "rouletteRoundId": 7,
    "cellId": 10
}
```

## Checking

It is handy to check app via Postman: exported Postman collection can be found here: https://gitlab.com/kozubskiy/roulette.installer/-/raw/master/docs/Roulette.postman_collection.json?inline=false

## Debugging

To use XDebug in PHPStorm for this `roulette` repository:

Uncomment these lines in `./.env.php-fpm`:
```ini
#PHP_EXTENSION_XDEBUG=1
#PHP_INI_XDEBUG__REMOTE_AUTOSTART=1
#PHP_INI_XDEBUG__REMOTE_PORT=9000
#PHP_INI_XDEBUG__REMOTE_ENABLE=1
#PHP_INI_XDEBUG__REMOTE_CONNECT_BACK=1
#PHP_INI_XDEBUG__REMOTE_MODE=req
#PHP_INI_XDEBUG__IDEKEY=PHPSTORM
#PHP_IDE_CONFIG=serverName=DockerLocalServer
```

run `make restart`

In PHPStorm: `Settings->Languages & Frameworks->PHP->Servers`
Add new server DockerLocalServer. Conigure:
Host = localhost
Port = 80
Debugger = Xdebug
Enable `Use path mappings` and set mapping:
%roulette_dir% -> `/var/www/html`
Close Settings.
`Run->Start listen to PHP debug connections`

[*] CodingMachine php image on startup runs 3 php scripts, so xdebug can obviously try to start debug session for these scripts. Just let them run (F9).  

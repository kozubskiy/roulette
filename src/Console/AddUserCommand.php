<?php

namespace App\Console;

use App\Database\Doctrine\Model\RouletteRound;
use App\Database\Doctrine\Model\User;
use App\Helpers\EntityManagerAutowiredTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddUserCommand extends Command
{
    use EntityManagerAutowiredTrait;

    protected function configure()
    {
        $this->setDescription('');
        $this->setName('app:add-user');
        $this->addArgument('email', InputArgument::REQUIRED);
        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $user = (new User())
            ->setEmail($input->getArgument('email'))
        ;

        $this->em->persist($user);
        $this->em->flush();
        $output->writeln('Created user with ID='.$user->getId());

        return 0;
    }
}

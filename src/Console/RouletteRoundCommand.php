<?php

namespace App\Console;

use App\Database\Doctrine\Model\RouletteRound;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RouletteRoundCommand extends Command
{
    protected function configure()
    {
        $this->setDescription('');
        $this->setName('app:round');
        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $round = new RouletteRound();
        do {
            $cellId = $round->turn();
            $output->writeln('Got '.$cellId);
        } while ($cellId !== RouletteRound::JACKPOT);

        $output->writeln('JACKPOT!');

        return 0;
    }
}

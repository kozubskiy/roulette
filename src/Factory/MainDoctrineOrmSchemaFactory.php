<?php


namespace App\Factory;

use CodexSoft\DatabaseFirst\DoctrineOrmSchema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Configuring Db-First code generation for main database
 */
class MainDoctrineOrmSchemaFactory
{
    public function __invoke(EntityManager $entityManager, KernelInterface $kernel): DoctrineOrmSchema
    {
        $baseNamespace = 'App\\Database\\';
        $basePath = $kernel->getProjectDir().'/src/Database';

        $ormSchema = (new DoctrineOrmSchema('App\\Database\\Main\\Doctrine'))
            ->setEntityManager($entityManager)

            ->setPathToPsrRoot($basePath.'/src')
            ->configureMigrations($baseNamespace.'Migrations', $basePath.'/Migrations')
            ->configureMapping($baseNamespace.'Doctrine\\Mapping', $basePath.'/Doctrine/Mapping')
            ->configureModels($baseNamespace.'Doctrine\\Model', $basePath.'/Doctrine/Model')
            ->configureModelsTraits($baseNamespace.'Doctrine\\Model\\Generated', $basePath.'/Doctrine/Model/Generated')
            ->configureModelsAwareTraits($baseNamespace.'Doctrine\\Model\\AwareTraits', $basePath.'/Doctrine/Model/AwareTraits')
            ->configureRepositories($baseNamespace.'Doctrine\\Repository', $basePath.'/Doctrine/Repository')
            ->configureRepositoriesTraits($baseNamespace.'Doctrine\\Repository\\Generated', $basePath.'/Doctrine/Repository/Generated')

            ->setOverwriteModelAwareTraits(true)

            /*
             * these tables will be skipped when mappng is being generated
             */
            ->setSkipTables([
                'doctrine_migration_versions',
            ])

        ;

        return $ormSchema;
    }
}

<?php


namespace App\Factory;


use App\Controller\AbstractJsonBaseAction;
use CodexSoft\JsonApi\JsonApiSchema;
use Symfony\Component\HttpKernel\KernelInterface;

class JsonBackendApiSchemaFactory
{
    public function __invoke(KernelInterface $kernel): JsonApiSchema
    {
        return (new JsonApiSchema())
            ->setBaseActionClass(AbstractJsonBaseAction::class)
            ->setNamespaceBase('')
            ->setNamespaceForms('App\\Controller\\Form')
            ->setNamespaceActions('App\\Controller')
            ->setPathToForms($kernel->getProjectDir().'/src/Controller/Form')
            ->setPathToActions($kernel->getProjectDir().'/src/Controller')
            ->setPathToPsrRoot($kernel->getProjectDir().'/src');
    }
}

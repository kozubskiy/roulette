<?php


namespace App\Factory;


use App\Helpers\Vars;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;

class MainConnectionFactory
{
    public function __invoke(): Connection
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $connection = DriverManager::getConnection([
            'url' => Vars::DATABASE_URL(),
            'driverOptions' => [
                \PDO::ATTR_TIMEOUT => Vars::DATABASE_CONNECTION_TIMEOUT(),
            ],
        ]);

        /*
         * We are using savepoints
         */
        $connection->setNestTransactionsWithSavepoints(true);

        return $connection;
    }
}

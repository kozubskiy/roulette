<?php


namespace App\Factory;


use App\Helpers\Vars;
use CodexSoft\DatabaseFirst\Orm\Postgres\AbstractPgSqlEntityManagerBuilder;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\HttpKernel\KernelInterface;

class MainEntityManagerFactory
{
    public function __invoke(KernelInterface $kernel, Connection $connection): EntityManager
    {
        $devMode = Vars::APP_ENV() === 'dev';

        $basePath = $kernel->getProjectDir().'/src/Database';
        $proxyDir = $kernel->getCacheDir().'/doctrine2-proxies-main';
        $cache = new \Doctrine\Common\Cache\ApcuCache();

        $configuration = Setup::createConfiguration($devMode, $proxyDir, $cache);

        $entityManager = (new class extends AbstractPgSqlEntityManagerBuilder {})
            ->setConfiguration($configuration)
            ->setIsDevMode($devMode)
            ->setMappingDirectories([$basePath.'/Doctrine/Mapping'])
            ->setConnection($connection)
            ->build();

        return $entityManager;
    }
}

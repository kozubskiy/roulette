<?php

namespace App\Controller;

use App\Contracts\UserInterface;
use App\Contracts\UserSessionInterface;
use App\Helpers\EntityManagerAutowiredTrait;
use App\Service\RouletteService;
use CodexSoft\JsonApi\DocumentedFormAction;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Base action for JSON API endpoints
 * Class AbstractJsonBaseAction
 *
 * @package App\Controller
 */
abstract class AbstractJsonBaseAction extends DocumentedFormAction
{
    use EntityManagerAutowiredTrait;

    protected static bool $decodeRequestJson = true;

    protected ?UserInterface $user;
    protected ?UserSessionInterface $userSession;
    protected ?RouletteService $rouletteService;

    //protected \League\Plates\Engine $plates;
    protected KernelInterface $kernel;

    // todo: optionally wrap whole action in transaction (try { handle() } catch {})?
    public function __construct(
        KernelInterface $kernel,
        RequestStack $requestStack,
        FormFactoryInterface $formFactory,
        RouletteService $rouletteService
        //\League\Plates\Engine $plates
    )
    {
        parent::__construct($requestStack, $formFactory);
        $this->kernel = $kernel;
        //$this->plates = $plates;
        $this->user = $this->request->attributes->get('user');
        $this->userSession = $this->request->attributes->get('userSession');
        $this->rouletteService = $rouletteService;
    }
}

<?php

namespace App\Controller\Roulette;

use CodexSoft\JsonApi\Form\AbstractForm;
use CodexSoft\JsonApi\Form\Field;
use CodexSoft\JsonApi\Documentation\Collector\Interfaces\SwagenInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;

class TurnRequestForm extends AbstractForm implements SwagenInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        Field::import($builder, [
            'userId' => Field::id('User ID'),
        ]);
    }
}

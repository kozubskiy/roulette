<?php

namespace App\Controller\Roulette;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("roulette/statistics", methods={"GET"})
 */
class StatisticsAction extends AbstractController
{
    private EntityManager $em;

    private function getMostActiveUsers()
    {
        $sql = 'SELECT u.id as user_id, COUNT(rr) as rounds_count, turns_count FROM users u
INNER JOIN roulette_rounds rr on u.id = rr.user_id
INNER JOIN (
    SELECT u.id as user_id, COUNT(*) as turns_count FROM users u
    INNER JOIN roulette_rounds rr on u.id = rr.user_id
    INNER JOIN roulette_round_turns rrt ON rr.id = rrt.roulette_round_id
    GROUP BY u.id
) turns ON u.id = turns.user_id
GROUP BY u.id, turns_count
ORDER BY turns_count DESC
LIMIT 10';

        $data = $this->em->getConnection()->executeQuery($sql)->fetchAll();
        foreach ($data as $key => $item) {
            $data[$key]['average_turns_per_round'] = (int) $item['turns_count'] / (int) $item['rounds_count'];
        }
        return $data;
    }

    private function getUsersPerRoundNum()
    {
        $sql = 'SELECT rr.ord_num, COUNT(rr.user_id) as users_count
FROM roulette_rounds rr
GROUP BY ord_num';

        return $this->em->getConnection()->executeQuery($sql)->fetchAll();
    }

    public function __invoke(EntityManager $em): Response
    {
        $this->em = $em;

        return new JsonResponse([
            'usersPerRoundNum' => $this->getUsersPerRoundNum(),
            'mostActive' => $this->getMostActiveUsers(),
        ]);
    }
}

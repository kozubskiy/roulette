<?php

namespace App\Controller\Roulette;

use App\Controller\AbstractJsonBaseAction;
use App\Database\Doctrine\Model\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("roulette/turn", methods={"POST"})
 */
class TurnAction extends AbstractJsonBaseAction
{
    public function handle(array $data, array $extraData = []): Response
    {
        $userId = $data['userId'];
        $user = $this->em->getRepository(User::class)->findOneBy([User::_id() => $userId]);

        if (!$user instanceof User) {
            return new JsonResponse('User with such id is not found', 500);
        }

        $round = $this->rouletteService->getLastNotFinishedRoundOrCreate($user);
        $cellId = $this->rouletteService->makeTurn($round);

        return new JsonResponse([
            'rouletteRoundId' => $round->getId(),
            'cellId' => $cellId,
        ]);
    }
}

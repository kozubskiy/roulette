<?php

namespace App\Service;

use App\Database\Doctrine\Model\RouletteRound;
use App\Database\Doctrine\Model\RouletteRoundTurn;
use App\Database\Doctrine\Model\User;
use App\Helpers\EntityManagerAutowiredTrait;
use CodexSoft\DatabaseFirst\Orm\Dql;

class RouletteService
{
    use EntityManagerAutowiredTrait;

    public const CELL_WEIGHTS = [
        1 => 20,
        2 => 100,
        3 => 45,
        4 => 70,
        5 => 15,
        6 => 140,
        7 => 20,
        8 => 20,
        9 => 140,
        10 => 45,
    ];

    //public function getLastNotFinishedRound(User $user): ?RouletteRound
    //{
    //    $qb = $this->em->getRepository(RouletteRound::class)->createQueryBuilder('rr');
    //    $qb->setMaxResults(1)->orderBy(RouletteRound::_ordNum('rr'), 'DESC');
    //
    //    Dql::requireAll($qb, [
    //        Dql::eq($qb, RouletteRound::_user('rr'), $user),
    //        Dql::eq($qb, RouletteRound::_isFinished('rr'), false),
    //    ]);
    //
    //    return $qb->getQuery()->getOneOrNullResult();
    //}

    public function getLastRound(User $user): ?RouletteRound
    {
        $qb = $this->em->getRepository(RouletteRound::class)->createQueryBuilder('rr');
        $qb->setMaxResults(1)->orderBy(RouletteRound::_ordNum('rr'), 'DESC');

        Dql::requireAll($qb, [
            Dql::eq($qb, RouletteRound::_user('rr'), $user),
        ]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getLastNotFinishedRoundOrCreate(User $user): RouletteRound
    {
        $lastRound = $this->getLastRound($user);
        if ($lastRound instanceof RouletteRound && $lastRound->getIsFinished() === false) {
            return $lastRound;
        }

        return $this->startNewRound($user, $lastRound);
    }

    public function startNewRound(User $user, ?RouletteRound $previousRound): RouletteRound
    {
        $round = (new RouletteRound())
            ->setIsFinished(false)
            ->setUser($user)
            ->setCreatedAt(new \DateTime());

        if ($previousRound) {
            $round->setOrdNum($previousRound->getOrdNum()+1);
        }

        $this->em->persist($round);
        $this->em->flush($round);

        return $round;
    }

    public function makeTurn(RouletteRound $round): int
    {
        $cellId = $round->turn();

        if ($cellId === RouletteRound::JACKPOT) {
            $round->setIsFinished(true);
        }

        $this->em->persist($round);

        $turn = (new RouletteRoundTurn())
            ->setRouletteRound($round)
            ->setLuckyCellId($cellId)
            ->setCreatedAt(new \DateTime());

        $this->em->persist($turn);

        $this->em->flush($round);

        return $cellId;
    }
}

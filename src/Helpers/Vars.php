<?php


namespace App\Helpers;

/**
 * Unified method getting ENV variables
 * @method static APP_BRANCH
 * @method static APP_DEBUG
 * @method static APP_ENV
 * @method static APP_LAST_COMMIT_TIMESTAMP
 * @method static APP_MAINTENANCE_MODE
 * @method static APP_SECRET
 * @method static argc
 * @method static argv
 * @method static CORS_ALLOW_ORIGIN
 * @method static DATABASE_CONNECTION_TIMEOUT
 * @method static DATABASE_URL
 * @method static DOCUMENT_ROOT
 * @method static DQL_DEBUG_MODE
 * @method static FAKE_DATA_GENERATION
 * @method static FILESYSTEM_MAIN_CLOUD_2_BUCKET
 * @method static FILESYSTEM_MAIN_CLOUD_BUCKET
 * @method static KERNEL_CLASS
 * @method static PATH_TRANSLATED
 * @method static PHP_INI_MAX_EXECUTION_TIME
 * @method static PHP_INI_MEMORY_LIMIT
 * @method static PHP_INI_POST_MAX_SIZE
 * @method static PHP_INI_UPLOAD_MAX_FILESIZE
 * @method static PHP_SELF
 * @method static PHP_VERSION
 * @method static APP_HOST
 * @method static REQUEST_TIME
 * @method static REQUEST_TIME_FLOAT
 * @method static SCRIPT_FILENAME
 * @method static SCRIPT_NAME
 * @method static SYMFONY_DEPRECATIONS_HELPER
 * @method static SYMFONY_DOTENV_VARS
 * @method static TRUSTED_HOSTS
 */
class Vars
{

    public const APP_BRANCH = 'APP_BRANCH';
    public const APP_DEBUG = 'APP_DEBUG';
    public const APP_ENV = 'APP_ENV';
    public const APP_LAST_COMMIT_TIMESTAMP = 'APP_LAST_COMMIT_TIMESTAMP';
    public const APP_MAINTENANCE_MODE = 'APP_MAINTENANCE_MODE';
    public const APP_SECRET = 'APP_SECRET';
    public const argc = 'argc';
    public const argv = 'argv';
    public const CORS_ALLOW_ORIGIN = 'CORS_ALLOW_ORIGIN';
    public const DATABASE_CONNECTION_TIMEOUT = 'DATABASE_CONNECTION_TIMEOUT';
    public const DATABASE_URL = 'DATABASE_URL';
    public const DOCUMENT_ROOT = 'DOCUMENT_ROOT';
    public const DQL_DEBUG_MODE = 'DQL_DEBUG_MODE';
    public const FAKE_DATA_GENERATION = 'FAKE_DATA_GENERATION';
    public const FILESYSTEM_MAIN_CLOUD_2_BUCKET = 'FILESYSTEM_MAIN_CLOUD_2_BUCKET';
    public const FILESYSTEM_MAIN_CLOUD_BUCKET = 'FILESYSTEM_MAIN_CLOUD_BUCKET';
    public const KERNEL_CLASS = 'KERNEL_CLASS';
    public const PATH_TRANSLATED = 'PATH_TRANSLATED';
    public const PHP_INI_MAX_EXECUTION_TIME = 'PHP_INI_MAX_EXECUTION_TIME';
    public const PHP_INI_MEMORY_LIMIT = 'PHP_INI_MEMORY_LIMIT';
    public const PHP_INI_POST_MAX_SIZE = 'PHP_INI_POST_MAX_SIZE';
    public const PHP_INI_UPLOAD_MAX_FILESIZE = 'PHP_INI_UPLOAD_MAX_FILESIZE';
    public const PHP_SELF = 'PHP_SELF';
    public const PHP_VERSION = 'PHP_VERSION';
    public const APP_HOST = 'APP_HOST';
    public const REQUEST_TIME = 'REQUEST_TIME';
    public const REQUEST_TIME_FLOAT = 'REQUEST_TIME_FLOAT';
    public const SCRIPT_FILENAME = 'SCRIPT_FILENAME';
    public const SCRIPT_NAME = 'SCRIPT_NAME';
    public const SYMFONY_DEPRECATIONS_HELPER = 'SYMFONY_DEPRECATIONS_HELPER';
    public const SYMFONY_DOTENV_VARS = 'SYMFONY_DOTENV_VARS';
    public const TRUSTED_HOSTS = 'TRUSTED_HOSTS';

    /**
     * @param $name
     * @param null $default
     *
     * @return mixed|null
     */
    public static function get(string $name, $default = null)
    {
        if (self::has($name)) {
            return self::getValue($name);
        }
        return $default;
    }

    private static function getValue(string $name)
    {
        return $_SERVER[$name];
    }

    public static function has(string $name): bool
    {
        return \array_key_exists($name, $_SERVER);
    }

    public static function set(string $name, $value): void
    {
        \putenv($name.'='.$_SERVER[$name] = $_ENV[$name] = $value);
    }

    public static function isset(string $name): bool
    {
        return self::has($name) && (self::getValue($name) !== null);
    }

    public static function empty(string $name): bool
    {
        return empty(self::get($name));
    }

    public static function nonEmpty(string $name): bool
    {
        return !self::empty($name);
    }

    public static function nonEmptyOrDefault(string $name, $default): bool
    {
        $value = self::get($name);
        return empty($value) ? $default : $value;
    }

    public static function __callStatic($name, $arguments)
    {
        return self::get($name);
    }
}

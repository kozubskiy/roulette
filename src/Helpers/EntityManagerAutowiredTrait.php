<?php


namespace App\Helpers;


use CodexSoft\DatabaseFirst\Helpers\WrappedEntityManager;
use Doctrine\ORM\EntityManager;

trait EntityManagerAutowiredTrait
{
    protected EntityManager $em;
    protected WrappedEntityManager $wem;

    /**
     * @required
     *
     * @param EntityManager $em
     *
     * @return static
     */
    public function setEm(EntityManager $em): self
    {
        $this->em = $em;
        $this->wem = new WrappedEntityManager($this->em);
        return $this;
    }
}

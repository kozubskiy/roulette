<?php

namespace App\Database\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * 
 * @method \App\Database\Doctrine\Model\RouletteRound|null find($id, $lockMode = null, $lockVersion = null)
 * @method \App\Database\Doctrine\Model\RouletteRound|null findOneBy(array $criteria, array $orderBy = null)
 * @method \App\Database\Doctrine\Model\RouletteRound[] findAll()
 * @method \App\Database\Doctrine\Model\RouletteRound[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouletteRoundRepository extends EntityRepository
{
    use \App\Database\Doctrine\Repository\Generated\RouletteRoundRepositoryTrait;
}
<?php

namespace App\Database\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * 
 * @method \App\Database\Doctrine\Model\RouletteRoundTurn|null find($id, $lockMode = null, $lockVersion = null)
 * @method \App\Database\Doctrine\Model\RouletteRoundTurn|null findOneBy(array $criteria, array $orderBy = null)
 * @method \App\Database\Doctrine\Model\RouletteRoundTurn[] findAll()
 * @method \App\Database\Doctrine\Model\RouletteRoundTurn[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouletteRoundTurnRepository extends EntityRepository
{
    use \App\Database\Doctrine\Repository\Generated\RouletteRoundTurnRepositoryTrait;
}
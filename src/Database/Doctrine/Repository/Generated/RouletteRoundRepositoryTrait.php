<?php

namespace App\Database\Doctrine\Repository\Generated;

use App\Database\Doctrine\Model;
use Doctrine\ORM\Query\Expr;
use CodexSoft\DatabaseFirst\Orm\Dql;

/**
 */
trait RouletteRoundRepositoryTrait
{
    /**
     * @param $createdAt
     * 
     *
     * @return null|Model\RouletteRound
     */
    public function getOneByCreatedAt($createdAt): ?Model\RouletteRound
    {
        return $this->findOneBy([Model\RouletteRound::_createdAt() => $createdAt]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $createdAt
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRound
     */
    public function getOneByCreatedAtOrFail($createdAt, $exceptionMessage = null): Model\RouletteRound
    {
        $rouletteRound = $this->getOneByCreatedAt($createdAt);
        if (!$rouletteRound instanceof Model\RouletteRound) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRound by createdAt');
        }
        return $rouletteRound;
    }
    
    /**
     * @param $createdAt
     * 
     *
     * @return Model\RouletteRound[]
     */
    public function getByCreatedAt($createdAt): array
    {
        return $this->findBy([Model\RouletteRound::_createdAt() => $createdAt]);
    }
    
    /**
     * @param $id
     * 
     *
     * @return null|Model\RouletteRound
     */
    public function getOneById($id): ?Model\RouletteRound
    {
        return $this->findOneBy([Model\RouletteRound::_id() => $id]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $id
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRound
     */
    public function getOneByIdOrFail($id, $exceptionMessage = null): Model\RouletteRound
    {
        $rouletteRound = $this->getOneById($id);
        if (!$rouletteRound instanceof Model\RouletteRound) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRound by id');
        }
        return $rouletteRound;
    }
    
    /**
     * @param $id
     * 
     *
     * @return Model\RouletteRound[]
     */
    public function getById($id): array
    {
        return $this->findBy([Model\RouletteRound::_id() => $id]);
    }
    
    /**
     * @param $isFinished
     * 
     *
     * @return null|Model\RouletteRound
     */
    public function getOneByIsFinished($isFinished): ?Model\RouletteRound
    {
        return $this->findOneBy([Model\RouletteRound::_isFinished() => $isFinished]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $isFinished
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRound
     */
    public function getOneByIsFinishedOrFail($isFinished, $exceptionMessage = null): Model\RouletteRound
    {
        $rouletteRound = $this->getOneByIsFinished($isFinished);
        if (!$rouletteRound instanceof Model\RouletteRound) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRound by isFinished');
        }
        return $rouletteRound;
    }
    
    /**
     * @param $isFinished
     * 
     *
     * @return Model\RouletteRound[]
     */
    public function getByIsFinished($isFinished): array
    {
        return $this->findBy([Model\RouletteRound::_isFinished() => $isFinished]);
    }
    
    /**
     * @param $ordNum
     * 
     *
     * @return null|Model\RouletteRound
     */
    public function getOneByOrdNum($ordNum): ?Model\RouletteRound
    {
        return $this->findOneBy([Model\RouletteRound::_ordNum() => $ordNum]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $ordNum
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRound
     */
    public function getOneByOrdNumOrFail($ordNum, $exceptionMessage = null): Model\RouletteRound
    {
        $rouletteRound = $this->getOneByOrdNum($ordNum);
        if (!$rouletteRound instanceof Model\RouletteRound) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRound by ordNum');
        }
        return $rouletteRound;
    }
    
    /**
     * @param $ordNum
     * 
     *
     * @return Model\RouletteRound[]
     */
    public function getByOrdNum($ordNum): array
    {
        return $this->findBy([Model\RouletteRound::_ordNum() => $ordNum]);
    }
    
    /**
     * @param $usedCellIds
     * 
     *
     * @return null|Model\RouletteRound
     */
    public function getOneByUsedCellIds($usedCellIds): ?Model\RouletteRound
    {
        return $this->findOneBy([Model\RouletteRound::_usedCellIds() => $usedCellIds]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $usedCellIds
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRound
     */
    public function getOneByUsedCellIdsOrFail($usedCellIds, $exceptionMessage = null): Model\RouletteRound
    {
        $rouletteRound = $this->getOneByUsedCellIds($usedCellIds);
        if (!$rouletteRound instanceof Model\RouletteRound) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRound by usedCellIds');
        }
        return $rouletteRound;
    }
    
    /**
     * @param $usedCellIds
     * 
     *
     * @return Model\RouletteRound[]
     */
    public function getByUsedCellIds($usedCellIds): array
    {
        return $this->findBy([Model\RouletteRound::_usedCellIds() => $usedCellIds]);
    }
    
    /**
     * @param int|Model\User $userOrId
     * 
     *
     * @return Model\RouletteRound[]
     */
    public function getByUser($userOrId): array
    {
        $userId = Model\User::extractId($userOrId);
        return $this->findBy([Model\RouletteRound::_user() => $userId]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * 
     * Получить ПЕРВУЮ ПОПАВШУЮСЯ подходящую сущность из всех подходящих, либо NULL
     * @param int|Model\User $userOrId
     *
     * @return null|Model\RouletteRound
     */
    public function getOneRandomByUser($userOrId): ?Model\RouletteRound
    {
        $userId = Model\User::extractId($userOrId);
        /** @var \Doctrine\ORM\EntityRepository $this */
        $qb = $this->createQueryBuilder('base');
        $qb->innerJoin(Model\User::class, 'joined', Expr\Join::WITH, Dql::andX($qb,[
            Dql::dql($qb,Model\RouletteRound::_user('base').' = joined'),
            Dql::eq($qb,Model\User::_id('joined'),$userId),
        ]));
        /** @noinspection PhpUnhandledExceptionInspection */
        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * 
     * Получить ПЕРВУЮ ПОПАВШУЮСЯ подходящую сущность из всех подходящих, либо бросить исключение
     * @param int|Model\User $userOrId
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     *
     * @return Model\RouletteRound
     */
    public function getOneRandomByUserOrFail($userOrId, $exceptionMessage = null): Model\RouletteRound
    {
        $user = $this->getOneRandomByUser($userOrId);
        if (!$user instanceof Model\RouletteRound) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRound by user');
        }
        return $user;
    }
    
    /**
     * 
     * @param int|Model\User $userOrId
     *
     * @return null|Model\RouletteRound
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneByUser($userOrId): ?Model\RouletteRound
    {
        $userId = Model\User::extractId($userOrId);
        /** @var \Doctrine\ORM\EntityRepository $this */
        $qb = $this->createQueryBuilder('base');
        $qb->innerJoin(Model\User::class, 'joined', Expr\Join::WITH, Dql::andX($qb,[
            Dql::dql($qb,Model\RouletteRound::_user('base').' = joined'),
            Dql::eq($qb,Model\User::_id('joined'),$userId),
        ]));
        return $qb->getQuery()->getOneOrNullResult();
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * 
     * @param int|Model\User $userOrId
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     *
     * @return Model\RouletteRound
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneByUserOrFail($userOrId, $exceptionMessage = null): Model\RouletteRound
    {
        $user = $this->getOneByUser($userOrId);
        if (!$user instanceof Model\RouletteRound) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRound by user');
        }
        return $user;
    }
    
    /**
     * Получить коллекцию, проиндексированную по ID, по массиву ID
     * @param int[] $ids
     *
     * @return Model\RouletteRound[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByIdsIndexedById(array $ids): array
    {
        if (\count($ids) === 0) {
            return [];
        }
    
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = $this->createQueryBuilder('base');
        $qb->indexBy('base', 'base.id');
        Dql::requireAll($qb, [
            Dql::in($qb, 'base.id', $ids),
        ]);
        return $qb->getQuery()->getResult();
    }
    
    }
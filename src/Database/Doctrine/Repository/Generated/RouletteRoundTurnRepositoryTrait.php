<?php

namespace App\Database\Doctrine\Repository\Generated;

use App\Database\Doctrine\Model;
use Doctrine\ORM\Query\Expr;
use CodexSoft\DatabaseFirst\Orm\Dql;

/**
 */
trait RouletteRoundTurnRepositoryTrait
{
    /**
     * @param $createdAt
     * 
     *
     * @return null|Model\RouletteRoundTurn
     */
    public function getOneByCreatedAt($createdAt): ?Model\RouletteRoundTurn
    {
        return $this->findOneBy([Model\RouletteRoundTurn::_createdAt() => $createdAt]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $createdAt
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRoundTurn
     */
    public function getOneByCreatedAtOrFail($createdAt, $exceptionMessage = null): Model\RouletteRoundTurn
    {
        $rouletteRoundTurn = $this->getOneByCreatedAt($createdAt);
        if (!$rouletteRoundTurn instanceof Model\RouletteRoundTurn) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRoundTurn by createdAt');
        }
        return $rouletteRoundTurn;
    }
    
    /**
     * @param $createdAt
     * 
     *
     * @return Model\RouletteRoundTurn[]
     */
    public function getByCreatedAt($createdAt): array
    {
        return $this->findBy([Model\RouletteRoundTurn::_createdAt() => $createdAt]);
    }
    
    /**
     * @param $id
     * 
     *
     * @return null|Model\RouletteRoundTurn
     */
    public function getOneById($id): ?Model\RouletteRoundTurn
    {
        return $this->findOneBy([Model\RouletteRoundTurn::_id() => $id]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $id
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRoundTurn
     */
    public function getOneByIdOrFail($id, $exceptionMessage = null): Model\RouletteRoundTurn
    {
        $rouletteRoundTurn = $this->getOneById($id);
        if (!$rouletteRoundTurn instanceof Model\RouletteRoundTurn) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRoundTurn by id');
        }
        return $rouletteRoundTurn;
    }
    
    /**
     * @param $id
     * 
     *
     * @return Model\RouletteRoundTurn[]
     */
    public function getById($id): array
    {
        return $this->findBy([Model\RouletteRoundTurn::_id() => $id]);
    }
    
    /**
     * @param $luckyCellId
     * 
     *
     * @return null|Model\RouletteRoundTurn
     */
    public function getOneByLuckyCellId($luckyCellId): ?Model\RouletteRoundTurn
    {
        return $this->findOneBy([Model\RouletteRoundTurn::_luckyCellId() => $luckyCellId]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $luckyCellId
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\RouletteRoundTurn
     */
    public function getOneByLuckyCellIdOrFail($luckyCellId, $exceptionMessage = null): Model\RouletteRoundTurn
    {
        $rouletteRoundTurn = $this->getOneByLuckyCellId($luckyCellId);
        if (!$rouletteRoundTurn instanceof Model\RouletteRoundTurn) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRoundTurn by luckyCellId');
        }
        return $rouletteRoundTurn;
    }
    
    /**
     * @param $luckyCellId
     * 
     *
     * @return Model\RouletteRoundTurn[]
     */
    public function getByLuckyCellId($luckyCellId): array
    {
        return $this->findBy([Model\RouletteRoundTurn::_luckyCellId() => $luckyCellId]);
    }
    
    /**
     * @param int|Model\RouletteRound $rouletteRoundOrId
     * 
     *
     * @return Model\RouletteRoundTurn[]
     */
    public function getByRouletteRound($rouletteRoundOrId): array
    {
        $rouletteRoundId = Model\RouletteRound::extractId($rouletteRoundOrId);
        return $this->findBy([Model\RouletteRoundTurn::_rouletteRound() => $rouletteRoundId]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * 
     * Получить ПЕРВУЮ ПОПАВШУЮСЯ подходящую сущность из всех подходящих, либо NULL
     * @param int|Model\RouletteRound $rouletteRoundOrId
     *
     * @return null|Model\RouletteRoundTurn
     */
    public function getOneRandomByRouletteRound($rouletteRoundOrId): ?Model\RouletteRoundTurn
    {
        $rouletteRoundId = Model\RouletteRound::extractId($rouletteRoundOrId);
        /** @var \Doctrine\ORM\EntityRepository $this */
        $qb = $this->createQueryBuilder('base');
        $qb->innerJoin(Model\RouletteRound::class, 'joined', Expr\Join::WITH, Dql::andX($qb,[
            Dql::dql($qb,Model\RouletteRoundTurn::_rouletteRound('base').' = joined'),
            Dql::eq($qb,Model\RouletteRound::_id('joined'),$rouletteRoundId),
        ]));
        /** @noinspection PhpUnhandledExceptionInspection */
        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * 
     * Получить ПЕРВУЮ ПОПАВШУЮСЯ подходящую сущность из всех подходящих, либо бросить исключение
     * @param int|Model\RouletteRound $rouletteRoundOrId
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     *
     * @return Model\RouletteRoundTurn
     */
    public function getOneRandomByRouletteRoundOrFail($rouletteRoundOrId, $exceptionMessage = null): Model\RouletteRoundTurn
    {
        $rouletteRound = $this->getOneRandomByRouletteRound($rouletteRoundOrId);
        if (!$rouletteRound instanceof Model\RouletteRoundTurn) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRoundTurn by rouletteRound');
        }
        return $rouletteRound;
    }
    
    /**
     * 
     * @param int|Model\RouletteRound $rouletteRoundOrId
     *
     * @return null|Model\RouletteRoundTurn
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneByRouletteRound($rouletteRoundOrId): ?Model\RouletteRoundTurn
    {
        $rouletteRoundId = Model\RouletteRound::extractId($rouletteRoundOrId);
        /** @var \Doctrine\ORM\EntityRepository $this */
        $qb = $this->createQueryBuilder('base');
        $qb->innerJoin(Model\RouletteRound::class, 'joined', Expr\Join::WITH, Dql::andX($qb,[
            Dql::dql($qb,Model\RouletteRoundTurn::_rouletteRound('base').' = joined'),
            Dql::eq($qb,Model\RouletteRound::_id('joined'),$rouletteRoundId),
        ]));
        return $qb->getQuery()->getOneOrNullResult();
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * 
     * @param int|Model\RouletteRound $rouletteRoundOrId
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     *
     * @return Model\RouletteRoundTurn
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneByRouletteRoundOrFail($rouletteRoundOrId, $exceptionMessage = null): Model\RouletteRoundTurn
    {
        $rouletteRound = $this->getOneByRouletteRound($rouletteRoundOrId);
        if (!$rouletteRound instanceof Model\RouletteRoundTurn) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find RouletteRoundTurn by rouletteRound');
        }
        return $rouletteRound;
    }
    
    /**
     * Получить коллекцию, проиндексированную по ID, по массиву ID
     * @param int[] $ids
     *
     * @return Model\RouletteRoundTurn[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByIdsIndexedById(array $ids): array
    {
        if (\count($ids) === 0) {
            return [];
        }
    
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = $this->createQueryBuilder('base');
        $qb->indexBy('base', 'base.id');
        Dql::requireAll($qb, [
            Dql::in($qb, 'base.id', $ids),
        ]);
        return $qb->getQuery()->getResult();
    }
    
    }
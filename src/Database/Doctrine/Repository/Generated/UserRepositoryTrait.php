<?php

namespace App\Database\Doctrine\Repository\Generated;

use App\Database\Doctrine\Model;
use Doctrine\ORM\Query\Expr;
use CodexSoft\DatabaseFirst\Orm\Dql;

/**
 */
trait UserRepositoryTrait
{
    /**
     * @param $email
     * 
     *
     * @return null|Model\User
     */
    public function getOneByEmail($email): ?Model\User
    {
        return $this->findOneBy([Model\User::_email() => $email]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $email
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\User
     */
    public function getOneByEmailOrFail($email, $exceptionMessage = null): Model\User
    {
        $user = $this->getOneByEmail($email);
        if (!$user instanceof Model\User) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find User by email');
        }
        return $user;
    }
    
    /**
     * @param $email
     * 
     *
     * @return Model\User[]
     */
    public function getByEmail($email): array
    {
        return $this->findBy([Model\User::_email() => $email]);
    }
    
    /**
     * @param $id
     * 
     *
     * @return null|Model\User
     */
    public function getOneById($id): ?Model\User
    {
        return $this->findOneBy([Model\User::_id() => $id]);
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param $id
     * @param string|\Exception|null $exceptionMessage a message for exception (or exception instance) that will raise if query fails
     * 
     *
     * @return Model\User
     */
    public function getOneByIdOrFail($id, $exceptionMessage = null): Model\User
    {
        $user = $this->getOneById($id);
        if (!$user instanceof Model\User) {
    
            if ($exceptionMessage instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $exceptionMessage */
                throw $exceptionMessage;
            }
    
            if (\is_string($exceptionMessage)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                throw new \DomainException($exceptionMessage);
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException('Failed to find User by id');
        }
        return $user;
    }
    
    /**
     * @param $id
     * 
     *
     * @return Model\User[]
     */
    public function getById($id): array
    {
        return $this->findBy([Model\User::_id() => $id]);
    }
    
    /**
     * Получить коллекцию, проиндексированную по ID, по массиву ID
     * @param int[] $ids
     *
     * @return Model\User[]
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getByIdsIndexedById(array $ids): array
    {
        if (\count($ids) === 0) {
            return [];
        }
    
        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = $this->createQueryBuilder('base');
        $qb->indexBy('base', 'base.id');
        Dql::requireAll($qb, [
            Dql::in($qb, 'base.id', $ids),
        ]);
        return $qb->getQuery()->getResult();
    }
    
    }
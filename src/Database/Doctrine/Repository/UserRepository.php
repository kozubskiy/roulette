<?php

namespace App\Database\Doctrine\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * 
 * @method \App\Database\Doctrine\Model\User|null find($id, $lockMode = null, $lockVersion = null)
 * @method \App\Database\Doctrine\Model\User|null findOneBy(array $criteria, array $orderBy = null)
 * @method \App\Database\Doctrine\Model\User[] findAll()
 * @method \App\Database\Doctrine\Model\User[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends EntityRepository
{
    use \App\Database\Doctrine\Repository\Generated\UserRepositoryTrait;
}
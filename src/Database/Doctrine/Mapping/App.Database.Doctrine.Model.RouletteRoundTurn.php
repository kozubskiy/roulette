<?php

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use \Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

/** @var Doctrine\ORM\Mapping\ClassMetadataInfo $metadata */

/** @noinspection PhpUnhandledExceptionInspection */
$metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_SEQUENCE);

$mapper = new ClassMetadataBuilder($metadata);
$mapper->setCustomRepositoryClass('App\Database\Doctrine\Repository\RouletteRoundTurnRepository');
$mapper->setTable('roulette_round_turns');

$mapper->createField('createdAt', Type::DATETIME)
    ->columnName('created_at')
    ->nullable()
    ->build();

$mapper->createField('id', Type::BIGINT)
    ->columnName('id')
    ->makePrimaryKey()
    ->option('unsigned',false)
    ->build();

$mapper->createField('luckyCellId', Type::SMALLINT)
    ->columnName('lucky_cell_id')
    ->nullable()
    ->option('unsigned',false)
    ->build();

$mapper->createManyToOne('rouletteRound', \App\Database\Doctrine\Model\RouletteRound::class)
    ->addJoinColumn('roulette_round_id', 'id')
    ->cascadePersist()
    ->cascadeRefresh()
    ->fetchLazy()
    ->build();

if (file_exists($_extraMappingInfoFile = __DIR__.'/../MappingOverride/'.basename(__FILE__))) {
    /** @noinspection PhpIncludeInspection */ include $_extraMappingInfoFile;
}
<?php

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use \Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

/** @var Doctrine\ORM\Mapping\ClassMetadataInfo $metadata */

/** @noinspection PhpUnhandledExceptionInspection */
$metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_SEQUENCE);

$mapper = new ClassMetadataBuilder($metadata);
$mapper->setCustomRepositoryClass('App\Database\Doctrine\Repository\UserRepository');
$mapper->setTable('users');

$mapper->createField('email', Type::STRING)
    ->columnName('email')
    ->length(50)
    ->option('fixed',false)
    ->build();

$mapper->createField('id', Type::BIGINT)
    ->columnName('id')
    ->makePrimaryKey()
    ->option('unsigned',false)
    ->build();

if (file_exists($_extraMappingInfoFile = __DIR__.'/../MappingOverride/'.basename(__FILE__))) {
    /** @noinspection PhpIncludeInspection */ include $_extraMappingInfoFile;
}
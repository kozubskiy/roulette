<?php

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use \Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

/** @var Doctrine\ORM\Mapping\ClassMetadataInfo $metadata */

/** @noinspection PhpUnhandledExceptionInspection */
$metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_SEQUENCE);

$mapper = new ClassMetadataBuilder($metadata);
$mapper->setCustomRepositoryClass('App\Database\Doctrine\Repository\RouletteRoundRepository');
$mapper->setTable('roulette_rounds');

$mapper->createField('createdAt', Type::DATETIME)
    ->columnName('created_at')
    ->nullable()
    ->build();

$mapper->createField('id', Type::BIGINT)
    ->columnName('id')
    ->makePrimaryKey()
    ->option('unsigned',false)
    ->build();

$mapper->createField('isFinished', Type::BOOLEAN)
    ->columnName('is_finished')
    ->option('default',false)
    ->build();

$mapper->createField('ordNum', Type::SMALLINT)
    ->columnName('ord_num')
    ->option('unsigned',false)
    ->option('default',1)
    ->build();

$mapper->createField('usedCellIds', 'smallint[]')
    ->columnName('used_cell_ids')
    ->option('default', [])
    ->build();

$mapper->createManyToOne('user', \App\Database\Doctrine\Model\User::class)
    ->addJoinColumn('user_id', 'id')
    ->cascadePersist()
    ->cascadeRefresh()
    ->fetchLazy()
    ->build();

if (file_exists($_extraMappingInfoFile = __DIR__.'/../MappingOverride/'.basename(__FILE__))) {
    /** @noinspection PhpIncludeInspection */ include $_extraMappingInfoFile;
}
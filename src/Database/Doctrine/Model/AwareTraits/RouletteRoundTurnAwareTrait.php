<?php

namespace App\Database\Doctrine\Model\AwareTraits;

use \App\Database\Doctrine\Model\RouletteRoundTurn;

trait RouletteRoundTurnAwareTrait
{
    
    /** @var RouletteRoundTurn */
    private $rouletteRoundTurn;
    
    /**
     * @param RouletteRoundTurn|int $rouletteRoundTurnOrId
     *
     * @return static
     */
    public function setRouletteRoundTurnOrId($rouletteRoundTurnOrId): self
    {
        $rouletteRoundTurn = RouletteRoundTurn::byId($rouletteRoundTurnOrId);
        $this->rouletteRoundTurn = $rouletteRoundTurn;
        return $this;
    }
    
}
<?php

namespace App\Database\Doctrine\Model\AwareTraits;

use \App\Database\Doctrine\Model\RouletteRound;

trait RouletteRoundAwareTrait
{
    
    /** @var RouletteRound */
    private $rouletteRound;
    
    /**
     * @param RouletteRound|int $rouletteRoundOrId
     *
     * @return static
     */
    public function setRouletteRoundOrId($rouletteRoundOrId): self
    {
        $rouletteRound = RouletteRound::byId($rouletteRoundOrId);
        $this->rouletteRound = $rouletteRound;
        return $this;
    }
    
}
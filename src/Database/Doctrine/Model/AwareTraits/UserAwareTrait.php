<?php

namespace App\Database\Doctrine\Model\AwareTraits;

use \App\Database\Doctrine\Model\User;

trait UserAwareTrait
{
    
    /** @var User */
    private $user;
    
    /**
     * @param User|int $userOrId
     *
     * @return static
     */
    public function setUserOrId($userOrId): self
    {
        $user = User::byId($userOrId);
        $this->user = $user;
        return $this;
    }
    
}
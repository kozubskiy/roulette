<?php

namespace App\Database\Doctrine\Model;
/**
 * 
 * RouletteRoundTurn
 * @method static \App\Database\Doctrine\Repository\RouletteRoundTurnRepository repo(\Doctrine\ORM\EntityManagerInterface $em = null)
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="App\Database\Doctrine\Repository\RouletteRoundTurnRepository")
 */
class RouletteRoundTurn
{
    use \App\Database\Doctrine\Model\Generated\RouletteRoundTurnTrait;
}
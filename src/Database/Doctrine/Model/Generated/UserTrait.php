<?php

namespace App\Database\Doctrine\Model\Generated;

trait UserTrait
{
    
    use \CodexSoft\DatabaseFirst\Orm\RepoStaticAccessTrait;



    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set email
     * 
     * 
     * @param string $email
     * @return static
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     * 
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get id
     * 
     * 
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    // to avoid human mistakes when using entity DQL properties names...
    
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _email(?string $alias = null): string { return \is_string($alias) ? $alias.'.email' : 'email'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _id(?string $alias = null): string { return \is_string($alias) ? $alias.'.id' : 'id'; }
    

    // to avoid human mistakes when using SQL table and columns names...
    
    public static function _db_table_($doubleQuoted = true): string { return $doubleQuoted ? '"users"' : 'users'; }
    
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_email(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."email"' : '"email"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_id(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."id"' : '"id"'; }
    
}
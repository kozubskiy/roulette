<?php

namespace App\Database\Doctrine\Model\Generated;

trait RouletteRoundTurnTrait
{
    
    use \CodexSoft\DatabaseFirst\Orm\RepoStaticAccessTrait;



    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer|null
     */
    private $luckyCellId;

    /**
     * @var \App\Database\Doctrine\Model\RouletteRound
     */
    private $rouletteRound;

    /**
     * Set createdAt
     * 
     * 
     * @param \DateTime|null $createdAt
     * @return static
     */
    public function setCreatedAt(?\datetime  $createdAt = null): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     * 
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * Get not-null createdAt or throw exception
     * 
     *
     * @param string|\Exception|null $message a message for exception (or exception instance) that will raise if createdAt is null
     * @return \DateTime
     */
    public function getCreatedAtOrFail($message = null): \DateTime
    {
        if (!$this->createdAt instanceof \DateTime) {
            if ($message instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $message */
                throw $message;
            }
    
            if ($message === null) {
                $message = 'RouletteRoundTurn.createdAt is null, but \DateTime was expected.';
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException($message);
        }
        return $this->createdAt;
    }
    

    /**
     * Get id
     * 
     * 
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set luckyCellId
     * 
     * 
     * @param integer|null $luckyCellId
     * @return static
     */
    public function setLuckyCellId(?int $luckyCellId = null): self
    {
        $this->luckyCellId = $luckyCellId;
        return $this;
    }

    /**
     * Get luckyCellId
     * 
     * 
     * @return integer
     */
    public function getLuckyCellId()
    {
        return $this->luckyCellId;
    }

    /**
     * Set rouletteRound
     * 
     * 
     * @param \App\Database\Doctrine\Model\RouletteRound|null $rouletteRound
     * @return static
     */
    public function setRouletteRound(?\App\Database\Doctrine\Model\RouletteRound  $rouletteRound = null): self
    {
        $this->rouletteRound = $rouletteRound;
        return $this;
    }

    /**
     * Get rouletteRound
     * 
     *
     * @return \App\Database\Doctrine\Model\RouletteRound|null
     */
    public function getRouletteRound(): ?\App\Database\Doctrine\Model\RouletteRound
    {
        return $this->rouletteRound;
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * Get not-null rouletteRound or throw exception
     * 
     *
     * @param string|\Exception|null $message a message for exception (or exception instance) that will raise if rouletteRound is null
     * @return \App\Database\Doctrine\Model\RouletteRound
     */
    public function getRouletteRoundOrFail($message = null): \App\Database\Doctrine\Model\RouletteRound
    {
        if (!$this->rouletteRound instanceof \App\Database\Doctrine\Model\RouletteRound) {
            if ($message instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $message */
                throw $message;
            }
    
            if ($message === null) {
                $message = 'RouletteRoundTurn.rouletteRound is null, but \App\Database\Doctrine\Model\RouletteRound was expected.';
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException($message);
        }
        return $this->rouletteRound;
    }
    

    // to avoid human mistakes when using entity DQL properties names...
    
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _createdAt(?string $alias = null): string { return \is_string($alias) ? $alias.'.createdAt' : 'createdAt'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _id(?string $alias = null): string { return \is_string($alias) ? $alias.'.id' : 'id'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _luckyCellId(?string $alias = null): string { return \is_string($alias) ? $alias.'.luckyCellId' : 'luckyCellId'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _rouletteRound(?string $alias = null): string { return \is_string($alias) ? $alias.'.rouletteRound' : 'rouletteRound'; }
    

    // to avoid human mistakes when using SQL table and columns names...
    
    public static function _db_table_($doubleQuoted = true): string { return $doubleQuoted ? '"roulette_round_turns"' : 'roulette_round_turns'; }
    
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_created_at(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."created_at"' : '"created_at"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_id(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."id"' : '"id"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_lucky_cell_id(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."lucky_cell_id"' : '"lucky_cell_id"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_roulette_round_id(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."roulette_round_id"' : '"roulette_round_id"'; }
    
}
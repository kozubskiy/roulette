<?php

namespace App\Database\Doctrine\Model\Generated;

trait RouletteRoundTrait
{
    
    use \CodexSoft\DatabaseFirst\Orm\RepoStaticAccessTrait;



    /**
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $isFinished = false;

    /**
     * @var integer
     */
    private $ordNum = 1;

    /**
     * @var integer[]
     */
    private $usedCellIds = array (
);

    /**
     * @var \App\Database\Doctrine\Model\User
     */
    private $user;

    /**
     * Set createdAt
     * 
     * 
     * @param \DateTime|null $createdAt
     * @return static
     */
    public function setCreatedAt(?\datetime  $createdAt = null): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     * 
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * Get not-null createdAt or throw exception
     * 
     *
     * @param string|\Exception|null $message a message for exception (or exception instance) that will raise if createdAt is null
     * @return \DateTime
     */
    public function getCreatedAtOrFail($message = null): \DateTime
    {
        if (!$this->createdAt instanceof \DateTime) {
            if ($message instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $message */
                throw $message;
            }
    
            if ($message === null) {
                $message = 'RouletteRound.createdAt is null, but \DateTime was expected.';
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException($message);
        }
        return $this->createdAt;
    }
    

    /**
     * Get id
     * 
     * 
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isFinished
     * 
     * 
     * @param boolean $isFinished
     * @return static
     */
    public function setIsFinished(bool $isFinished): self
    {
        $this->isFinished = $isFinished;
        return $this;
    }

    /**
     * Get isFinished
     * 
     * 
     * @return boolean
     */
    public function getIsFinished()
    {
        return $this->isFinished;
    }

    /**
     * Set ordNum
     * 
     * 
     * @param integer $ordNum
     * @return static
     */
    public function setOrdNum(int $ordNum): self
    {
        $this->ordNum = $ordNum;
        return $this;
    }

    /**
     * Get ordNum
     * 
     * 
     * @return integer
     */
    public function getOrdNum()
    {
        return $this->ordNum;
    }

    /**
     * Set usedCellIds
     * 
     * 
     * @param integer[] $usedCellIds
     * @return static
     */
    public function setUsedCellIds(array $usedCellIds): self
    {
        $this->usedCellIds = $usedCellIds;
        return $this;
    }

    /**
     * Get usedCellIds
     * 
     * 
     * @return integer[]
     */
    public function getUsedCellIds()
    {
        return $this->usedCellIds;
    }

    /**
     * Set user
     * 
     * 
     * @param \App\Database\Doctrine\Model\User|null $user
     * @return static
     */
    public function setUser(?\App\Database\Doctrine\Model\User  $user = null): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     * 
     *
     * @return \App\Database\Doctrine\Model\User|null
     */
    public function getUser(): ?\App\Database\Doctrine\Model\User
    {
        return $this->user;
    }
    
    /**
     * @noinspection PhpDocMissingThrowsInspection
     * Get not-null user or throw exception
     * 
     *
     * @param string|\Exception|null $message a message for exception (or exception instance) that will raise if user is null
     * @return \App\Database\Doctrine\Model\User
     */
    public function getUserOrFail($message = null): \App\Database\Doctrine\Model\User
    {
        if (!$this->user instanceof \App\Database\Doctrine\Model\User) {
            if ($message instanceof \Exception) {
                /** @noinspection PhpUnhandledExceptionInspection */
                /** @var \RuntimeException $message */
                throw $message;
            }
    
            if ($message === null) {
                $message = 'RouletteRound.user is null, but \App\Database\Doctrine\Model\User was expected.';
            }
    
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new \DomainException($message);
        }
        return $this->user;
    }
    

    // to avoid human mistakes when using entity DQL properties names...
    
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _createdAt(?string $alias = null): string { return \is_string($alias) ? $alias.'.createdAt' : 'createdAt'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _id(?string $alias = null): string { return \is_string($alias) ? $alias.'.id' : 'id'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _isFinished(?string $alias = null): string { return \is_string($alias) ? $alias.'.isFinished' : 'isFinished'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _ordNum(?string $alias = null): string { return \is_string($alias) ? $alias.'.ordNum' : 'ordNum'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _usedCellIds(?string $alias = null): string { return \is_string($alias) ? $alias.'.usedCellIds' : 'usedCellIds'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _user(?string $alias = null): string { return \is_string($alias) ? $alias.'.user' : 'user'; }
    

    // to avoid human mistakes when using SQL table and columns names...
    
    public static function _db_table_($doubleQuoted = true): string { return $doubleQuoted ? '"roulette_rounds"' : 'roulette_rounds'; }
    
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_created_at(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."created_at"' : '"created_at"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_id(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."id"' : '"id"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_is_finished(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."is_finished"' : '"is_finished"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_ord_num(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."ord_num"' : '"ord_num"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_used_cell_ids(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."used_cell_ids"' : '"used_cell_ids"'; }
    
    /**
     * @param string|null $alias
     * @return string
     */
    public static function _db_user_id(?string $alias = null): string { return \is_string($alias) ? '"'.$alias.'"."user_id"' : '"user_id"'; }
    
}
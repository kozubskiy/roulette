<?php

namespace App\Database\Doctrine\Model;
use App\Contracts\UserInterface;

/**
 *
 * User
 * @method static \App\Database\Doctrine\Repository\UserRepository repo(\Doctrine\ORM\EntityManagerInterface $em = null)
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="App\Database\Doctrine\Repository\UserRepository")
 */
class User implements UserInterface
{
    use \App\Database\Doctrine\Model\Generated\UserTrait;
}

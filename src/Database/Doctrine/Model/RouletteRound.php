<?php

namespace App\Database\Doctrine\Model;
use App\Service\RouletteService;

/**
 *
 * RouletteRound
 * @method static \App\Database\Doctrine\Repository\RouletteRoundRepository repo(\Doctrine\ORM\EntityManagerInterface $em = null)
 * @Doctrine\ORM\Mapping\Entity(repositoryClass="App\Database\Doctrine\Repository\RouletteRoundRepository")
 */
class RouletteRound
{
    use \App\Database\Doctrine\Model\Generated\RouletteRoundTrait;

    public const JACKPOT = 0;

    /**
     * @return int
     * @throws \Exception
     */
    public function turn(): int
    {
        if (\count(RouletteService::CELL_WEIGHTS) === \count($this->usedCellIds)) {
            return self::JACKPOT;
        }

        $usedCellIds = $this->usedCellIds;

        $cells = \array_filter(RouletteService::CELL_WEIGHTS, function($cellId) use ($usedCellIds) {
            return !\in_array($cellId, $usedCellIds, true);
        }, ARRAY_FILTER_USE_KEY);

        $allSum = \array_sum($cells);
        $reals = [];
        foreach ($cells as $cellId => $cellWeight) {
            $maxElementProbability = $cellWeight / $allSum;
            $realElementLuck = $maxElementProbability * \random_int(0, 100);
            $reals[$cellId] = $realElementLuck;
        }

        \arsort($reals);

        $gotId = \array_keys($reals)[0];
        $this->usedCellIds[] = $gotId;
        return $gotId;
    }

}

CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    email VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE roulette_rounds (
    id BIGSERIAL PRIMARY KEY,
    ord_num SMALLINT NOT NULL DEFAULT 1,
    user_id BIGINT REFERENCES users NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    used_cell_ids SMALLINT[] DEFAULT '{}' NOT NULL,
    is_finished BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE roulette_round_turns (
    id BIGSERIAL PRIMARY KEY,
    roulette_round_id BIGINT REFERENCES roulette_rounds NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    lucky_cell_id SMALLINT
);

ALTER TABLE roulette_rounds ADD CONSTRAINT roulette_rounds__deny_duplicates UNIQUE (ord_num, user_id);

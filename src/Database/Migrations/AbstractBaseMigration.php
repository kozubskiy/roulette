<?php

namespace App\Database\Migrations;

class AbstractBaseMigration extends \CodexSoft\DatabaseFirst\Migration\BaseMigration
{
    public static function getContainingDirectory(): string
    {
        return __DIR__;
    }
}